Create the bootable media
=========================

The easiest way to install Linux Mint is with a USB stick.

If you cannot boot from USB, you can use a blank DVD.

How to make a bootable USB stick
--------------------------------

In Linux Mint
`````````````

Right-click the ISO file and select :menuselection:`Make Bootable USB Stick`, or launch :menuselection:`Menu --> Accessories --> USB Image Writer`.

.. figure:: images/mintstick.png
    :width: 500px
    :align: center

Select your USB device and click :guilabel:`Write`.

In Windows, Mac OS, or other Linux distributions
````````````````````````````````````````````````

Download `Etcher <https://etcher.io/>`_, install it and run it.

.. figure:: images/etcher.png
    :width: 500px
    :align: center

    Using Etcher


    .. figure:: images/rufus.png
    :width: 500px
    :align: center

Click :guilabel:`Select image` and select your ISO file.

Click :guilabel:`Select drive` and select your USB stick.

Click :guilabel:`Flash!`.

other viable alternitives

Download `rufus <https://rufus.ie/en_US/>`_, install it and run it.




click on your flash drive you want to boot Linux Mint too

click select and select the ISO file

Click on mbr or gpt find out which one you have go press win = X then disk manage and click on your drive and it will say if is is mbr or gpt 

click on start ans it will flash your device



    .. figure:: images/Universal-USB-Installer.png
    :width: 500px
    :align: center


Download `Universal USB Installer <https://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/>`_, install it and run it.



select the linux distribution

select browse and select the ISO

click on Create 


.. figure:: images/UNetbootin_icon.svg
    :width: 500px
    :align: center




Download `UNetbootin <https://unetbootin.github.io/>`_, install it and run it.


Click on distribution and select version and set those to the latest version


click on ... and select the iso location

click on ok


How to make a bootable DVD
--------------------------

Optical discs are slow and burning to disc is prone to errors.

.. note::
	To prevent issues, burn at the lowest possible speed.

.. warning::
	Burn the content of the ISO onto the DVD, not the ISO file itself. When finished, your DVD should contain directories such as ``boot`` and ``casper``, it shouldn't be an empty DVD containing an .iso file.

In Linux
````````
Install and use ``xfburn``.

In Windows
``````````
Right-click the ISO file and select :menuselection:`Burn disk image`.

To make sure the ISO was burned without any errors, select :menuselection:`Verify disc after burning`.

In Mac OS
`````````
Right-click the ISO file and select :menuselection:`Burn Disk Image to Disc`.
